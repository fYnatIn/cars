Up dockers: docker-compose up


Basic server is available on http://localhost:3001


Fill test data: docker exec -it carlist-app node fill-test-data.js

Upload script cron (every hour): 0 * * * * docker exec -it carlist-app node upload.js > /dev/null 2>&1

Mongo shell is available at: docker exec -it mongodb mongo