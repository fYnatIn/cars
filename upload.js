require('dotenv').config()

const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB, { useMongoClient: true })

const CarModel = require('./car')(mongoose)

mongoose.connection.on('open', () => {
    async function upload (cars) {
        if (cars.length) {
            console.log(cars.length, 'cars to upload')
            const data = await Promise.all(cars.map(car => car.upload(car)))
            const updated = await CarModel.update({_id: {$in: data.filter(id => id !== null)}}, {$set: {Uploaded: true}}, {multi:true})
            console.log(updated.nModified, 'cars uploaded')
        } else {
            console.log('No cars to upload')
        }

        return mongoose.connection.close()
    }

    CarModel.find({Uploaded: false}, (err, cars) => {
        if (err) {
            console.log(err)
            return mongoose.connection.close()
        }

        return upload(cars)
    })
})