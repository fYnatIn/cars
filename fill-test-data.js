require('dotenv').config()

const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB, { useMongoClient: true })

const CarModel = require('./car')(mongoose)

mongoose.connection.on('open', () => {
    CarModel.insertMany(require('./test-cars.json'), (err, docs) => {
        if (err) {
            console.log(err)
        } else {
            console.log('Done!')
            mongoose.connection.close()
        }
    })

})
