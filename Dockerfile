FROM node:8.6

WORKDIR /home/carlist

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3001

CMD ["npm", "start"]