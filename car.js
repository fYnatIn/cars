var request = require('request')

module.exports = mongoose => {
    const carSchema = mongoose.Schema({
        sellingId: String,
        Brand: String,
        Model: String,
        Title: String,
        Price: Number,
        Uploaded: {type: Boolean, default: false}
    })

    carSchema.methods.upload = function () {
        return new Promise((resolve, reject) => {
            console.log(this.Brand, this.Model, 'is uploading...')
            request.post({
                url: 'http://wispy-bird-8767.getsandbox.com/cars',
                json: {
                    sellingId: this.sellingId,
                    Brand: this.Brand,
                    Model: this.Model,
                    Title: this.Title,
                    Price: this.Price
                }
            }, (err, res, body) => {
                if (err) {
                    return reject(err)
                }
                if (res.statusCode === 200) {
                    resolve(this._id)
                } else {
                    resolve(null)
                }
            })
        })
    }

    const Model = mongoose.model('car', carSchema)

    return Model
}
