require('dotenv').config()

const http = require('http')
const mongoose = require('mongoose')
const CarModel = require('./car')(mongoose)

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB, { useMongoClient: true })
mongoose.connection.on('open', () => {
    http.createServer(function (req, res) {
        CarModel.find({})
            .then(data => {
                data.forEach(car => {
                    res.write(car.Brand + ' ' + car.Model + ' - ' + car.Title + ': ' + car.Price + '\n')
                })
                res.end()
            }).catch(err => {
                res.write(err)
                res.end()
            })
    }).listen(3001)
})